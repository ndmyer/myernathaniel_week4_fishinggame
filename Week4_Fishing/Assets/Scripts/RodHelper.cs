﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RodHelper : MonoBehaviour
{
    public Transform fishPosition;
    public GameObject helperRod;
    public FishBrain FishBrain;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (FishBrain.isTired == false)
        {
            float x = fishPosition.position.x;
            float y = fishPosition.position.y;
            float angle = 0f;

            angle = Mathf.Atan2(x, y + 5f) * Mathf.Rad2Deg;
        
            helperRod.transform.rotation = Quaternion.Euler(helperRod.transform.rotation.x, helperRod.transform.rotation.x, angle);
        }
        else
        {
            helperRod.transform.rotation = Quaternion.Euler(helperRod.transform.rotation.x, helperRod.transform.rotation.x, 0);
        }
        
        
        
    }
}
