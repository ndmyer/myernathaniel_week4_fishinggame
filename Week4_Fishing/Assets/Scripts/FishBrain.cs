﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishBrain : MonoBehaviour
{
    public float fishMaxStamina;
    public float fishCurrentStamina;
    public float fishStaminaRegen;
    public bool isMoving = false;
    public bool isTired = false;
    public SpriteRenderer fishSprite;

    public int _movementState;

    public Transform _fishReference;
    


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating(nameof(fishBrain), 1f, .01f);
    }

    // Update is called once per frame
    void Update()
    {
        if (isTired == true)
        {
            //spawn particles
        }
    }

    private void fishBrain()
    {
        var time = 0f;
        time += Time.deltaTime;

        if (fishCurrentStamina > 0 && isTired == false)
        {
            fishCurrentStamina -= .1f;
            
            if (isMoving == false)
            {
                _movementState = Random.Range(1, 5);
                time = 0f;
            }
            
            if (_movementState == 1)
            {
                isMoving = true;
    
                _fishReference.Translate(new Vector3(-1f,1)/50);
    
                //float angle = (Mathf.Atan2(-1, 1) * Mathf.Rad2Deg)-90;
                //_fishReference.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                if (fishCurrentStamina <= 0f)
                {
                    
                }
                else if (time >= .2f)
                {
                    isMoving = false;
                }
            }
            else if (_movementState == 2)
            {
                isMoving = true;
                
                _fishReference.Translate(new Vector3(1,1)/50);
                
                //float angle = (Mathf.Atan2(1, 1) * Mathf.Rad2Deg)-90;
                //_fishReference.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                
                if (fishCurrentStamina <= 0f)
                {
                    
                }
                else if (time >= .2f)
                {
                    isMoving = false;
                }
            }
            else if (_movementState == 3)
            {
                isMoving = true;
                
                _fishReference.Translate(new Vector3(-1,.2f)/50);
                
                //float angle = (Mathf.Atan2(1, 1) * Mathf.Rad2Deg)-90;
                //_fishReference.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                
                if (fishCurrentStamina <= 0f)
                {
                    
                }
                else if (time >= .2f)
                {
                    isMoving = false;
                }
            }
            else if (_movementState == 4)
            {
                isMoving = true;
                
                _fishReference.Translate(new Vector3(1,.2f)/50);
                
                //float angle = (Mathf.Atan2(1, 1) * Mathf.Rad2Deg)-90;
                //_fishReference.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                
                if (fishCurrentStamina <= 0f)
                {
                    
                }
                else if (time >= .2f)
                {
                    isMoving = false;
                }
            }
        }
        else
        {
            isTired = true;
            fishSprite.color = Color.yellow;
            
            isMoving = false;
            fishCurrentStamina += fishStaminaRegen;
            
            if (fishCurrentStamina >= fishMaxStamina)
            {
                isTired = false;
                fishSprite.color = Color.white;
            }
        }
        

    }
    
}
