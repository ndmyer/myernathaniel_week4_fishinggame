﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingLine : MonoBehaviour
{
    public Transform rodTransform;
    public Transform fishTransform;
    public Player player;
    public FishBrain fishBrain;
    public Material fishingLineMat;
    public Color fullHealthLine;
    public Color noHealthLine;

    public LineRenderer lineRend;

    public float fishinglineLength;
    private float fishDistance;
    private float _reelRotation;
    public float _fishingLineHealth = 100;
    private bool _isTired;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fishingLineMat.color = Color.Lerp(fullHealthLine, noHealthLine, 1 - _fishingLineHealth / 100);
        
        //ReelInFishingLine();
        
        AdjustFishPosititon();
        
        DrawFishingLine(rodTransform, fishTransform);
    }

    private void ReelInFishingLine()
    {
    }

    private void AdjustFishPosititon()
    {
        fishDistance = Vector3.Distance(rodTransform.position, fishTransform.position);

        if (fishDistance > fishinglineLength)
        {
            var direction = rodTransform.position - fishTransform.position;
            
            fishTransform.Translate(direction.normalized/30);
            //fishTransform.position = Vector3.Lerp(fishTransform.position, direction.normalized, .01f);
        }
    }

    private void DrawFishingLine(Transform currRodTransform, Transform currFishTransform)
    {
        lineRend.SetPosition(0, currRodTransform.position);
        lineRend.SetPosition(1, currFishTransform.position);
    }
}
