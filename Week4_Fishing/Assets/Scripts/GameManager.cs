﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject fish;
    public FishBrain fishBrain;
    public GameObject blackScreenObject;
    private Image blackScreenImage;
    public FishingLine fishingLine;
    public GameObject particalesPrefab;
    public Text fishinglineLengthText;
    public Text fishCaughtText;

    private int fishCaught;
    private bool gameEnding = false;
    private bool particlesActive = false;
    private GameObject particles;
    
    
    // Start is called before the first frame update
    void Start()
    {
        blackScreenImage = blackScreenObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        //fishinglineLengthText.text = "Fishing Line Length: " + fishingLine.fishinglineLength;
        fishinglineLengthText.text = string.Format("Fishing Line Length: {0:#00.0}", fishingLine.fishinglineLength);

        fishCaughtText.text = "Fish Caught: " + fishCaught;
        
        /*
        if (fishBrain.isTired == true && particlesActive == false)
        {
            particlesActive = true;
            particles = Instantiate(particalesPrefab, fish.transform.position, quaternion.identity);
        }
        else
        {
            if (particles != null)
                Destroy(particles);
            
            particlesActive = false;
        }
        */

        if (fishingLine.fishinglineLength <= .1f)
        {
            fishCaught++;
            fishingLine.fishinglineLength = 5f;
            fish.transform.position = new Vector3(Random.Range(-8f, 8f), Random.Range(0f, 3.5f), 0f);
        }

        if (fishingLine._fishingLineHealth <= 0 && gameEnding == false)
        {
            //Debug.Log("Game Over");
            //GameOver();
            
            SceneManager.LoadScene(0);
            gameEnding = true;
        }
    }
    
    public IEnumerator GameOver()
    {
        FadeToBlack();
        
        yield return new WaitForSeconds(seconds: 2f);
        
        Application.LoadLevel(index: Application.loadedLevel);
    }
    
    void FadeToBlack ()
    {
        blackScreenObject.SetActive(true);
        blackScreenImage.color = Color.black;
        blackScreenImage.canvasRenderer.SetAlpha(0.0f);
        blackScreenImage.CrossFadeAlpha (1.0f, 2f, false);
    }
}
