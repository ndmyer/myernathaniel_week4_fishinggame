﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public GameObject fishingRod;
    public GameObject fishingRodAnchor;
    public GameObject helperRodAnchor;
    public GameObject fishingReel;
    public Vector2 pullingVector2;
    public Vector2 reelingVector2;
    public FishingLine fishingLine;
    public FishBrain fishBrain;

    public float reelRotation;
    private float prevAngle = 360f;
    private float currAngle = 0f;
    private float totalAngle = 0f;
    private int rotations = 0;

    private float _rodDistance;


    private float reelVibrationTime = 0f;
    private float reelVibrationDistance = 0f;
    private float reelVibrationSpeed = 0f;
    private float pullVibrationSpeed = 0f;
    private float pullRodDistance = 0f;
    private bool isReeling = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isReeling == true)
            reelVibrationTime += Time.deltaTime;

        pullRodDistance = Vector3.Distance(fishingRodAnchor.transform.position, helperRodAnchor.transform.position);

        pullVibrationSpeed = Mathf.Clamp(pullRodDistance / 2, 0f, 1f);
        
        
        Gamepad.current.SetMotorSpeeds(pullVibrationSpeed, reelVibrationSpeed);

        if (fishBrain.isTired == false && pullRodDistance > .3f)
        {
            fishingLine.fishinglineLength =
                Mathf.Clamp(fishingLine.fishinglineLength + pullRodDistance / 1000, 0.1f, 10f);
        }
        
        if (pullRodDistance <= .3f || fishBrain.isTired == true)
        {
            pullVibrationSpeed = 0f;
            Gamepad.current.SetMotorSpeeds(pullVibrationSpeed, reelVibrationSpeed);
        }
    }

    public void OnPulling(InputAction.CallbackContext context)
    {
        pullingVector2 = context.ReadValue<Vector2>();
        float x = pullingVector2.x;
        float y = pullingVector2.y;
        float angle = 0f;

        if (x != 0.0f || y != 0.0f)
            angle = Mathf.Atan2(-x + .0f, -y + .8f) * Mathf.Rad2Deg;

        fishingRod.transform.rotation = Quaternion.Euler(fishingRod.transform.rotation.x, fishingRod.transform.rotation.y, angle);
    }
    
    public void OnReeling(InputAction.CallbackContext context)
    {
        //Gamepad.current.SetMotorSpeeds(.9f, 0f);

        isReeling = true;
        //reelVibrationTime += Time.deltaTime;
        
        reelingVector2 = context.ReadValue<Vector2>();
        float x = reelingVector2.x;
        float y = reelingVector2.y;
        float newRotate = 0f;
        
        if (x != 0.0f || y != 0.0f)
        {
            currAngle = Mathf.Atan2(x, y) * Mathf.Rad2Deg;

            if (currAngle < 0)
                currAngle += 360f;
            
            if (currAngle - 35 > prevAngle)
            {
                prevAngle = 360f;
            }

            newRotate = Mathf.Clamp(currAngle - prevAngle, 0f, 360f);
            //totalAngle = totalAngle + Mathf.Clamp(currAngle - prevAngle, 0f, 360f);
            reelVibrationDistance += newRotate;
            reelVibrationSpeed = Mathf.Clamp(reelVibrationDistance/ 1300 / reelVibrationTime, 0f, 1f);
            totalAngle += newRotate;
            prevAngle = currAngle;
            
            Gamepad.current.SetMotorSpeeds(0f, reelVibrationSpeed);
        }

        fishingLine.fishinglineLength = Mathf.Clamp(fishingLine.fishinglineLength - newRotate / 1000, 0.1f, 10f);
        
        if (fishBrain.isTired == false)
        {
            fishingLine._fishingLineHealth -= newRotate / 30;
        }

        reelRotation = totalAngle;

        /*
        if (totalAngle >= 360)
        {
            totalAngle = 0;
            rotations += 1;
        }
        */
        
        //fishingReel.transform.rotation = Quaternion.Euler(fishingRod.transform.rotation.x, fishingRod.transform.rotation.y, currAngle);
        fishingReel.transform.Rotate(new Vector3(0, 0, -newRotate));

        if (x == 0f || y == 0f)
        {
            reelVibrationDistance = 0f;
            isReeling = false;
            reelVibrationTime = 0f;
            reelVibrationSpeed = 0f;
            Gamepad.current.SetMotorSpeeds(pullVibrationSpeed, reelVibrationSpeed);
            //InputSystem.PauseHaptics();
        }
    }
}
